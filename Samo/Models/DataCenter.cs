using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Samo.Models
{
    public class DataCenter
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string CountryID { get; set; }
        public Country Country { get; set; }

        public ICollection<Server> Servers { get; set; }
    }
}
