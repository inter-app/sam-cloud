﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Samo.Data;
using Samo.Models;
using Samo.Areas.Identity;

namespace Samo.Controllers
{
    public class DataCentersController : Controller
    {
        private readonly SamoContext _context;

        public DataCentersController(SamoContext context)
        {
            _context = context;
        }

        // GET: DataCenters
        public async Task<IActionResult> Index(string countryFilter, string nameFilter)
        {
            if (!AuthenticationManager.isAdmin(User, _context))
                return View("_UnAuthorized");
            ViewData["countryFilter"] = countryFilter;
            ViewData["nameFilter"] = nameFilter;

            var dataCenter =  _context.DataCenter.Include(d => d.Country);

            var samoContext = from s in dataCenter select s;

            if (!String.IsNullOrEmpty(countryFilter))
                samoContext = samoContext.Where(s => s.Country.Name.Contains(countryFilter));
            if (!String.IsNullOrEmpty(nameFilter))
                samoContext = samoContext.Where(s => s.Name.Contains(nameFilter));


            return View(await samoContext.ToListAsync());
        }

        // GET: DataCenters/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (!AuthenticationManager.isAdmin(User, _context))
                return View("_UnAuthorized");
            if (id == null)
                return NotFound();

            var dataCenter = await _context.DataCenter.Include(d => d.Country).FirstOrDefaultAsync(m => m.ID == id);
            
            if (dataCenter == null)
                return NotFound();

            return View(dataCenter);
        }

        // GET: DataCenters/Create
        public IActionResult Create()
        {
            if (!AuthenticationManager.isAdmin(User, _context))
                return View("_UnAuthorized");

            ViewData["CountryName"] = new SelectList(_context.Country, "ID", "Name");

            return View();
        }

        // POST: DataCenters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,CountryID")] DataCenter dataCenter)
        {
            var isDataCenterExistInCountry =  _context.DataCenter.Any(d => d.CountryID == dataCenter.CountryID);

            if (isDataCenterExistInCountry) {
                ViewData["ErrorMessage"] = "Can't add more than one data center in the same country";
            }

            if (ModelState.IsValid && !isDataCenterExistInCountry)
            {
                _context.Add(dataCenter);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            ViewData["CountryName"] = new SelectList(_context.Country, "ID", "Name", dataCenter.CountryID);

            return View(dataCenter);
        }

        // GET: DataCenters/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (!AuthenticationManager.isAdmin(User, _context))
                return View("_UnAuthorized");
            if (id == null)
                return NotFound();

            var dataCenter = await _context.DataCenter.FindAsync(id);
            if (dataCenter == null)
                return NotFound();

            ViewData["CountryName"] = new SelectList(_context.Country, "ID", "Name");

            return View(dataCenter);
        }

        // POST: DataCenters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ID,Name,CountryID")] DataCenter dataCenter)
        {
            if (id != dataCenter.ID)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dataCenter);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DataCenterExists(dataCenter.ID))
                        return NotFound();
                    else
                        throw;
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["CountryName"] = new SelectList(_context.Country, "ID", "Name", dataCenter.CountryID);

            return View(dataCenter);
        }

        // GET: DataCenters/Delete/5
        public async Task<IActionResult> Delete(string id, bool? saveChangesError = false)
        {
            if (!AuthenticationManager.isAdmin(User, _context))
                return View("_UnAuthorized");
            if (id == null)
                return NotFound();

            var dataCenter = await _context.DataCenter.Include(d => d.Country).FirstOrDefaultAsync(m => m.ID == id);
            if (dataCenter == null)
                return NotFound();

            if (saveChangesError.GetValueOrDefault())
                ViewData["ErrorMessage"] = "The deletion failed. Contact the administrator please.";

            return View(dataCenter);
        }


        // POST: DataCenters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var dataCenter = await _context.DataCenter.FindAsync(id);
            _context.DataCenter.Remove(dataCenter);
            try
            {
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                return RedirectToAction(nameof(Delete), new { id = id, saveChangesError = true });
            }

        }

        private bool DataCenterExists(string id)
        {
            return _context.DataCenter.Any(e => e.ID == id);
        }
    }
}
