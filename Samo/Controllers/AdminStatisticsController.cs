﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Samo.Data;
using Samo.Models;
using System.Text;
using Samo.Areas.Identity;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Samo.Controllers
{
    public class AdminStatisticsController : Controller
    {
        private readonly SamoContext _context;

        public AdminStatisticsController(SamoContext context)
        {
            _context = context;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            if (!AuthenticationManager.isAdmin(User, _context))
                return View("_UnAuthorized");
            return View();
        }

        public JsonResult OSCountStats()
        {
            List<OsCountData> stats = new List<OsCountData>();
            var data = _context.Servers.GroupBy(s => s.OS);
            foreach (var os in data)
            {
                stats.Add(new OsCountData(){Name = os.Key.ToString(),Count = os.Count()});
            }

            return Json(stats);

        }

        public JsonResult ServersPerUser()
        {
            List<UserServerData> stats = new List<UserServerData>();
            var data = _context.Servers.Include(s => s.User).ToList().GroupBy(s => s.User);

            foreach (var user in data)
            {
                stats.Add(new UserServerData{User = user.Key.UserName,Count = user.Count()});
            }
            return Json(stats);
        }

        public JsonResult ServerPerDC()
        {
            List<DatacenterData> stats = new List<DatacenterData>();
            var data = _context.Servers.Include(s => s.DataCenter).ToList().GroupBy(s => s.DataCenter);

            foreach (var datacenter in data)
            {
                stats.Add(new DatacenterData{Name = datacenter.Key.Name,Count = datacenter.Count()});
            }
            return Json(stats);
        }
    }

    public class DatacenterData
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }

    public class UserServerData
    {
        public string User { get; set; }
        public int Count { get; set; }
    }

    public class OsCountData
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
