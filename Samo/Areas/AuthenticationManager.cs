﻿using System.Linq;
using Samo.Data;

namespace Samo.Areas.Identity
{
    public class AuthenticationManager
    {
        public AuthenticationManager()
        {

        }

        public static bool isAdmin(dynamic currentUser, SamoContext _context)
        {
            string currentUserName = currentUser.Identity.Name;
            if (currentUserName != null)
            {
                var user = _context.Users.SingleOrDefault(u => u.UserName == currentUserName || u.Email == currentUserName);
                if (user != null && user.isAdmin)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool isPermitted(dynamic currentUser, string creatorID, SamoContext _context)
        {
            string currentUserName = currentUser.Identity.Name;
            if (currentUser.Identity.Name == null)
            {
                return false;
            }

            var user = _context.Users.SingleOrDefault(u => u.UserName == currentUserName);

            if (user != null && (isAdmin(currentUser, _context) || creatorID == user.Id))
            {
                return true;
            }

            return false;
        }
    }
}
