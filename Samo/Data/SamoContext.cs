using Samo.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Samo.Data
{
    public class SamoContext : IdentityDbContext
	{
        public SamoContext(DbContextOptions<SamoContext> options) : base(options)
        {
        }

        public DbSet<Server> Servers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Samo.Models.DataCenter> DataCenter { get; set; }
		public DbSet<Samo.Models.Country> Country { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			// Customize the ASP.NET Identity model and override the defaults if needed.
			// For example, you can rename the ASP.NET Identity table names and more.
			// Add your customizations after calling base.OnModelCreating(builder);

			builder.Entity<User>() //Use your application user class here
				   .ToTable("AspNetUsers"); //Set the table name here
		}
	}
}
