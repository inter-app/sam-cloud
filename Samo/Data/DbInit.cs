using Samo.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;


namespace Samo.Data
{
    public static class DbInitializer
    {
        public static void Initialize(SamoContext context)
        {
            context.Database.EnsureCreated();

            // Look for any users.
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }

            var users = new User[]
            {
                new User{
                    Id ="9e749b66-631a-4088-95ff-c3592b927db7",
                    UserName ="shira@gmail.com",
                    NormalizedUserName="SHIRA@GMAIL.COM",
                    SecurityStamp="E5UVWQZS6ZSIVFOYPCVZGTLUNK3CC7EF",
                    ConcurrencyStamp="1afbc73e-b2d7-46a1-bce3-0b450ef5958c",
                    PasswordHash="AQAAAAEAACcQAAAAECYX0WQkDDOAsS9HbvKfQjUtgIv7eeBJGTXroJ/dv/4xVqiIT2CDQfMMdSLqTg35yA==",
                    isAdmin=true
                },

                new User{
                    Id ="8093c30d-4a69-4e22-9f80-1804dba74963",
                    UserName ="mor@gmail.com",
                    NormalizedUserName="MOR@GMAIL.COM",
                    SecurityStamp="MKBBFJFW4DHLMMDMXYA2R5WVCWB4OKBO",
                    ConcurrencyStamp="282ec21e-d828-4985-9005-a1f96ac29a81",
                    PasswordHash="AQAAAAEAACcQAAAAEJpy/RV3I+xNZCXrnlLxpfapDJ14ktFMK2qhzGfxzLMsOTIoNZ3a5wmfN0olpeI26w==",
                    isAdmin=true
                }
            };
            foreach (User u in users)
            {
                context.Users.Add(u);
            }
            context.SaveChanges();

            var Contries = new Country[]
            {
            new Country{ID="1", Name="Israel" },
            new Country{ID="2", Name="France" },
            new Country{ID="3", Name="Greece"},
            new Country{ID="4", Name="China"},
            new Country{ID="5", Name="Thailand"},
            new Country{ID="6", Name="Spain"},
            new Country{ID="7", Name="Bulgary"},
            new Country{ID="8", Name="India"},
            };

            foreach (Country c in Contries)
            {
                context.Country.Add(c);
            }
            
            context.SaveChanges();
        }
    }
}

